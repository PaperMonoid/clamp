# clamp

A lisp programming language made in [javacc](https://github.com/javacc/javacc).

# How to compile
Run the following command in the lexical analyzer directory to compile it:

```shell
# compile with javacc
java -cp ../../../../dist/javacc-6.0/bin/lib/javacc.jar javacc LexicalAnalyzer.jj

# compile with java
javac LexicalAnalyzer.java

# parse a file
java LexicalAnalyzer < Simple1.clamp
```
