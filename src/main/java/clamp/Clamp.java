package clamp;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

import parser.Parser;
import parser.ParserConstants;
import parser.Token;
import parser.SimpleCharStream;

public class Clamp {
    public static void main(String[] args) {
	Token token = null;
	try {
	    File file = new File(args[0]);
	    Parser parser = new Parser(new FileInputStream(file));
	    System.out.println(String.format("Comenzando análisis sintáctico en \"%s\" ...", args[0]));
	    parser.exprs();
	    token = Parser.getNextToken();
	    if (token.kind != ParserConstants.EOF) {
		if (token.kind == ParserConstants.CLOSEGROUP) {
		    throw new RuntimeException("Token inválido");
		}
		throw new RuntimeException("Token inválido");
	    }
	    System.out.println();
	    System.out.println("Se encontraron 0 errores");
	    System.out.println();
	    System.out.println("Tabla de símbolos:");
	    System.out.println(Parser.table);
	} catch (IOException exception) {
	    System.out.println(String.format("No se pudo leer el archivo \"%s\"", args[0]));
	} catch (RuntimeException exception) {
	    System.out.println();
	    String message = "foo";
	    String line = "";
	    String column = "";
	    String cerca = "";
	    String puntero = "";
	    try {
		List<String> lines = Files.readAllLines(Paths.get(args[0]));
		line = String.format("%d", SimpleCharStream.getBeginLine());
		int i = SimpleCharStream.getBeginLine() - 1;
		cerca = lines.get(i);
		column = String.format("%d", SimpleCharStream.getBeginColumn());
		int j =  SimpleCharStream.getBeginColumn() - 1;
		for (int k = 0; k < j; k++) {
		    puntero += " ";
		}
		puntero += "^";
	    } catch(Exception secondException) {
		System.out.println(String.format("No se pudo leer el archivo \"%s\"", args[0]));
	    }
	    System.out.println(String.format("Error de sintáxis en la línea %s y columna %s cerca de:\n%s\n%s", line, column, cerca, puntero));
	    System.out.println(String.format("No se esperaba:\n%s", token.image));
	} catch (Exception exception) {
	    System.out.println();
	    String message = exception.getMessage();
	    String first = message.substring(0, message.indexOf("Was expecting one of:"));
	    String second = message.substring(message.indexOf("Was expecting one of:") + 22, message.length());
	    String line = first.substring(first.indexOf("line") + 5, first.indexOf("column") - 2);
	    String column = first.substring(first.indexOf("column") + 7, first.length() - 2);
	    String cerca = "";
	    String puntero = "";
	    try {
		List<String> lines = Files.readAllLines(Paths.get(args[0]));
		int i = Integer.parseInt(line) - 1;
		cerca = lines.get(i);
		int j = Integer.parseInt(column) - 1;
		for (int k = 0; k < j; k++) {
		    puntero += " ";
		}
		puntero += "^";
	    } catch(Exception secondException) {
		System.out.println(String.format("No se pudo leer el archivo \"%s\"", args[0]));
	    }
	    System.out.println(String.format("Error de sintáxis en la línea %s y columna %s cerca de:\n%s\n%s", line, column, cerca, puntero));
	    System.out.println(String.format("Se esperaba:\n%s", second));

	}
    }
}
